import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TUTDFparserTest
{
    private static TUTDFparser parser1;
    private static DateFormat df = new SimpleDateFormat("yyyyMMdd");
    @BeforeClass
    public static void initParser()
    {
        parser1 = new TUTDFparser("files"+ File.separator+"TUTDF Sample v 4.0 - BC_bankruptcy_20160805.txt");
    }
    @Test
    public void testTHeader()
    {
        assertTrue(parser1.isValid());
        THeader tHeader = parser1.gettHeader();
        assertEquals("TUTDF",tHeader.getSegmentName());
        assertEquals("4.0r",tHeader.getVersion());
        assertEquals("1301ZZ013006",tHeader.getMemberCode());
        assertEquals("4d9e6s8w",tHeader.getAuthCode());
        try
        {
            assertEquals(df.parse("20150701"),tHeader.getVersionDate());
            assertEquals(df.parse("20160102"),tHeader.getReportedDate());
        } catch (ParseException e)
        {
            e.printStackTrace();
        }

    }
}
