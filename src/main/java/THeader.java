import java.io.Serializable;
import java.util.Date;

public class THeader implements Serializable
{
    private String segmentName;
    private String version;
    private Date versionDate;
    private String memberCode;
    private String cycleID;
    private Date reportedDate;
    private String authCode;
    private String memberData;

    public THeader()
    {
    }

    public String getSegmentName()
    {
        return segmentName;
    }

    public void setSegmentName(String segmentName)
    {
        this.segmentName = segmentName;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public Date getVersionDate()
    {
        return versionDate;
    }

    public void setVersionDate(Date versionDate)
    {
        this.versionDate = versionDate;
    }

    public String getMemberCode()
    {
        return memberCode;
    }

    public void setMemberCode(String memberCode)
    {
        this.memberCode = memberCode;
    }

    public String getCycleID()
    {
        return cycleID;
    }

    public void setCycleID(String cycleID)
    {
        this.cycleID = cycleID;
    }

    public Date getReportedDate()
    {
        return reportedDate;
    }

    public void setReportedDate(Date reportedDate)
    {
        this.reportedDate = reportedDate;
    }

    public String getAuthCode()
    {
        return authCode;
    }

    public void setAuthCode(String authCode)
    {
        this.authCode = authCode;
    }

    public String getMemberData()
    {
        return memberData;
    }

    public void setMemberData(String memberData)
    {
        this.memberData = memberData;
    }


}
