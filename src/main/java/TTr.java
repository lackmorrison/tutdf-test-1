import java.io.Serializable;
import java.util.Date;

public class TTr implements Serializable
{
    private String segmentName;
    private String memberCode;
    private String accountNumber;
    private int accountType;
    private int accountRelationship;
    private Date accountOpenedDate;
    private Date lastPaymentDate;
    private int accountRating;
    private Date accountRatingDate;
    private Date reportedDate;
    private int creditLimit;
    private int balance;
    private int pastDue;
    private int nextPayment;
    private int paymentFrequency;
    private String mop;
    private String currencyCode;
    private int collateralCode;
    private Date contractTerminationDate;
    private Date paymentDueDate;
    private Date interestPaymentDueDate;
    private int interestPaymentFrequency;
    private String oldMemberCode;
    private String oldAccountNumber;
    private int amountOutstanding;
    private boolean hasGuarantor;
    private boolean isFullyGuarantorSecured;
    private int guaranteeSum;
    private Date guaranteeTerm;
    private boolean hasBankGuarantee;
    private boolean ifFullyBankSecured;
    private int bankGuaranteeSum;
    private Date bankGuaranteeTerm;
    private int collateralValue;
    private Date collateralDate;
    private Date collateralAgreementExpirationDate;
    private int overallCreditValue;
    private String rightOfClaimAcquiersNames;
    private String rightOfClaimAcquiersRegistration;
    private String acquiersTaxpayerID;
    private String acquiersSocialInsurance;
    private Date completePerformanceDate;

    public TTr()
    {
    }

}
