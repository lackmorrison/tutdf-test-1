import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TUTDFparser
{
    final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
    private String filePath;
    private boolean valid;
    private THeader tHeader;
    private ArrayList<TPhone> tPhones;
    private TName tName;
    private TFinal tFinal;
    private ArrayList<TId> tIds;
    private TTr tTr;

    public TUTDFparser(String filePath)
    {
        this.filePath = filePath;
        valid = true;
        parse(filePath);
        if (valid)
        {
            validate();
        }
    }

    public boolean isValid()
    {
        return valid;
    }

    private void parse(String filePath)
    {
        String data;
        try
        {
            data = new String(Files.readAllBytes(Paths.get(filePath)),"Cp1251");
        } catch (IOException e)
        {
            valid = false;
            e.printStackTrace();
            return;
        }

        String line = null;
        try ( BufferedReader bufferedReader = new BufferedReader(new StringReader(data)))
        {
            while((line = bufferedReader.readLine())!=null && valid)
            {
                String [] contents = line.split("\t");
                String field_0 = contents[0];
                System.out.println(field_0);
                switch (field_0)
                {
                    case "TUTDF":
                        parseTHeader(contents);
                        break;
                    case "NA01":
                        parseTName(contents);
                        break;
                    case "TR01":
                        parseTTr(contents);
                        break;
                    case "TRLR":
                        parseTFinal(contents);
                        break;
                    default:
                        if (field_0.matches("ID\\d{2}"))
                            parseTId(contents);
                        if (field_0.matches("PN\\d{2}"))
                            parseTPhone(contents);
                }
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void validate()
    {

    }
    private void parseTHeader(String[] contents)
    {
        if (tHeader!=null||contents.length<7)
        {
            System.out.println("invalid format (THeader)");
            valid = false;
            return;
        }

        Date versionDate;
        Date reportedDate;
        tHeader = new THeader();
        tHeader.setSegmentName(contents[0]);
        tHeader.setVersion(contents[1]);
        try
        {
            versionDate = DATE_FORMAT.parse(contents[2]);
            tHeader.setVersionDate(versionDate);
            reportedDate = DATE_FORMAT.parse(contents[5]);
            tHeader.setReportedDate(reportedDate);
        }catch(ParseException e)
        {
            e.printStackTrace();
            valid = false;
            return;
        }
        tHeader.setMemberCode(contents[3]);
        tHeader.setCycleID(contents[4]);
        tHeader.setAuthCode(contents[6]);
        if (contents.length==8)
        {
            tHeader.setMemberData(contents[7]);
        }
        System.out.println();
    }
    private void parseTName(String[] contents)
    {

    }
    private void parseTTr(String[] contents)
    {

    }
    private void parseTFinal(String[] contents)
    {

    }
    private void parseTId(String[] contents)
    {

    }
    private void parseTPhone(String[] contents)
    {

    }

    public THeader gettHeader()
    {
        return tHeader;
    }

    public ArrayList<TPhone> gettPhones()
    {
        return tPhones;
    }

    public TName gettName()
    {
        return tName;
    }

    public TFinal gettFinal()
    {
        return tFinal;
    }

    public ArrayList<TId> gettIds()
    {
        return tIds;
    }

    public TTr gettTr()
    {
        return tTr;
    }
}
