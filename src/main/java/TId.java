import java.io.Serializable;
import java.util.Date;

public class TId implements Serializable
{
    private String segmentTag;
    private int idType;
    private String seriesNumber;
    private String idNumber;
    private Date issueDate;
    private String issueAuthority;
    private String issueLocation;

    public TId()
    {
    }

    public String getSegmentTag()
    {
        return segmentTag;
    }

    public void setSegmentTag(String segmentTag)
    {
        this.segmentTag = segmentTag;
    }

    public int getIdType()
    {
        return idType;
    }

    public void setIdType(int idType)
    {
        this.idType = idType;
    }

    public String getSeriesNumber()
    {
        return seriesNumber;
    }

    public void setSeriesNumber(String seriesNumber)
    {
        this.seriesNumber = seriesNumber;
    }

    public String getIdNumber()
    {
        return idNumber;
    }

    public void setIdNumber(String idNumber)
    {
        this.idNumber = idNumber;
    }

    public Date getIssueDate()
    {
        return issueDate;
    }

    public void setIssueDate(Date issueDate)
    {
        this.issueDate = issueDate;
    }

    public String getIssueAuthority()
    {
        return issueAuthority;
    }

    public void setIssueAuthority(String issueAuthority)
    {
        this.issueAuthority = issueAuthority;
    }

    public String getIssueLocation()
    {
        return issueLocation;
    }

    public void setIssueLocation(String issueLocation)
    {
        this.issueLocation = issueLocation;
    }
}
