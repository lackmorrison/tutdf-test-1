import java.io.Serializable;

public class TFinal implements Serializable
{
    private String trailer;
    private int counter;


    public TFinal()
    {
    }

    public String getTrailer()
    {
        return trailer;
    }

    public void setTrailer(String trailer)
    {
        this.trailer = trailer;
    }

    public int getCounter()
    {
        return counter;
    }

    public void setCounter(int counter)
    {
        this.counter = counter;
    }

}
