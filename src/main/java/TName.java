import java.io.Serializable;
import java.util.Date;

public class TName implements Serializable
{
    private String segmentName;
    private String surname;
    private String patronymicName;
    private String firstName;
    private Date birthday;
    private String birthdayLocation;
    private boolean deceased;
    private String oldSurname;
    private String oldFirstName;

    public TName()
    {
    }

    public String getSegmentName()
    {
        return segmentName;
    }

    public void setSegmentName(String segmentName)
    {
        this.segmentName = segmentName;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public String getPatronymicName()
    {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName)
    {
        this.patronymicName = patronymicName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public Date getBirthday()
    {
        return birthday;
    }

    public void setBirthday(Date birthday)
    {
        this.birthday = birthday;
    }

    public String getBirthdayLocation()
    {
        return birthdayLocation;
    }

    public void setBirthdayLocation(String birthdayLocation)
    {
        this.birthdayLocation = birthdayLocation;
    }

    public boolean isDeceased()
    {
        return deceased;
    }

    public void setDeceased(boolean deceased)
    {
        this.deceased = deceased;
    }

    public String getOldSurname()
    {
        return oldSurname;
    }

    public void setOldSurname(String oldSurname)
    {
        this.oldSurname = oldSurname;
    }

    public String getOldFirstName()
    {
        return oldFirstName;
    }

    public void setOldFirstName(String oldFirstName)
    {
        this.oldFirstName = oldFirstName;
    }

}
